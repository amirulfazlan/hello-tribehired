<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // 1. Fetch all posts from API Endpoint

        $all_post_res = Http::get('https://jsonplaceholder.typicode.com/posts');
        $all_comment_res = Http::get('https://jsonplaceholder.typicode.com/comments');

        $all_post = $all_post_res->json();

        $sorted_post = array();

        foreach($all_post as $post){
            $post_id = $post['id'];

            // 2. Count number of comments for all posts

            $all_comment = $all_comment_res->json();

            // 2.1 Filter all comments for particular post

            $new = array_filter($all_comment, function ($var) use ($post_id) {
                return ($var['postId'] == $post_id);
            });

            // 2.2 Count numner of comments

            $comment_count = count($new);

            $post_detail = [
                'post_id' => $post_id,
                'post_title' => $post['title'],
                'post_body' => $post['body'],
                'total_number_of_comments' => $comment_count,
                // 'post_comment' => $new
            ];



            array_push($sorted_post, $post_detail);
        };

        usort($sorted_post, function($a, $b) {
            return $a['total_number_of_comments'] - $b['total_number_of_comments'];
        });




        return response()->json($sorted_post, 200);
    }

    public function searchComment(Request $request){

        $search_criteria = $request['search_criteria'];
        $search_parameter = $request['search_parameter'];

        if ( isset($search_criteria) && isset($search_parameter)) {

        // Get all comments 

        $all_comment_res = Http::get('https://jsonplaceholder.typicode.com/comments');
        $all_comment = $all_comment_res->json();

        $comment_filter = array_filter($all_comment, function ($var) use ($search_criteria, $search_parameter) {
            return ($var[$search_criteria] == $search_parameter);
        });






            $response = [
                'search_criteria' => $request['search_criteria'],
                'search_parameter' => $request['search_parameter'],
                'search_result' => $comment_filter
            ];
            return response()->json($response, 200); 

        } else {
            $warning = [
                'Message'=>'Error! Please Input Criteria And Parameter Before Searching!'
            ];
            return response()->json($warning, 500);
        }

 
    }
}
